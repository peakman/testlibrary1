import XCTest
@testable import TestLibrary1

final class TestLibrary1Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TestLibrary1().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
