import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TestLibrary1Tests.allTests),
    ]
}
#endif