import XCTest

import TestLibrary1Tests

var tests = [XCTestCaseEntry]()
tests += TestLibrary1Tests.allTests()
XCTMain(tests)